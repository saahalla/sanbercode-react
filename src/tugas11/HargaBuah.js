import React from 'react';
import '../App.css';

let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]

class NamaBuah extends React.Component {
  render(){
    return (
      <td>{this.props.nama}</td>
    )
  }
}
class HargaBuah extends React.Component {
  render(){
    return (
      <td>{this.props.harga}</td>
    )
  }
}
class BeratBuah extends React.Component {
  render(){
    return (
      <td>{this.props.berat}</td>
    )
  }
}
class Tugas11 extends React.Component {
  render(){
		return (
		  <>
        <h1 style={{textAlign: "center"}}>Table Harga Buah</h1>
        <table style={{width: "100%", border: "2px solid black"}}>
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
            </tr>
          </thead>
            {dataHargaBuah.map(item=>{
              return (

              <tbody>
              <tr>
              <NamaBuah nama={item.nama} />
              <HargaBuah harga={item.harga} />
              <BeratBuah berat={item.berat/1000 + "kg"} />
              </tr>
              </tbody>

              )
            })}
      </table>
      </>
		)
	}
}

export default Tugas11